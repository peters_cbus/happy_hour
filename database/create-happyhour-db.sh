#!/bin/bash
BASEDIR=$(dirname $0)
psql -U postgres -f "$BASEDIR/dropdb.sql" &&
createdb -U postgres happyhour &&
psql -U postgres -d happyhour -f "$BASEDIR/schema.sql" &&
psql -U postgres -d happyhour -f "$BASEDIR/user.sql" &&
psql -U postgres -d happyhour -f "$BASEDIR/data.sql"